package com.example.service;

import com.example.model.Claim;
import org.activiti.engine.task.Task;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/applicationContext.xml"})
public class ClaimWorkflowTest {

    @Autowired
    public RuntimeService runtimeService;
    @Autowired
    public RepositoryService repositoryService;
    @Autowired
    public TaskService taskService;
    @Autowired
    public IdentityService identityService;

    public HashMap<String, Object> variables = new HashMap<String, Object>();

  /*  @Before
    public void init(){
        claimRemoverTest();
    }

    @After
    public void destroy(){
        claimRemoverTest();
    }
*/

    @Test
    public void smallAmountTest() {
        Claim claim = new Claim("New Claim", 1000.00, "Open");
        variables.put("claim", claim);
        runtimeService.startProcessInstanceByKey("claimStandardWorkflow", variables);
        assertEquals("Closed", claim.getStatus());
    }

    @Test
    public void bigAmountDifferentUserTest() {
        List<Task> taskList = getTaskListForUser("kermit");
        assertEquals(0, taskList.size());
        for (Task task : taskList)
            taskService.complete(task.getId());
    }

    @Test
    public void bigAmountCustomUserWithManyTaskTest() {
        List<Task> taskList = getTaskListForUser("harry");
        assertEquals(1, taskList.size());
        for (Task task : taskList)
            taskService.complete(task.getId());
    }

    @Test
    public void bigAmountCustomUserWithOneTaskTest() {
        List<Task> taskList = getTaskListForUser("michael");
        assertEquals(1, taskList.size());
        for (Task task : taskList)
            taskService.complete(task.getId());
    }

    @Test
    public void bigAmountCustomUserWithoutTaskTest() {
        List<Task> taskList = getTaskListForUser("tom");
        assertEquals(0, taskList.size());
        for (Task task : taskList)
            taskService.complete(task.getId());
    }

    //TODO: Remove method
    @Test
      public void claimRemoverTest() {
        identityService.setAuthenticatedUserId("harry");
        List<Task> taskList = taskService.createTaskQuery().taskCandidateUser("michael").taskName("Clarify claim").list();
        for (Task task : taskList){
            taskService.setVariable(task.getId(),"user","michael");
            taskService.complete(task.getId());
        }
        identityService.setAuthenticatedUserId("harry");
        List<Task> taskList1 = taskService.createTaskQuery().taskCandidateUser("michael").taskName("Process claim").list();
        for (Task task : taskList1){
            taskService.setVariable(task.getId(),"user","michael");
            taskService.complete(task.getId());
        }
    }

    private List<Task> getTaskListForUser(String userId) {
        Claim claim = new Claim("New Claim", 4000.00, "Open");
        variables.put("claim", claim);
        variables.put("group", "denialManagement");
        runtimeService.startProcessInstanceByKey("claimStandardWorkflow", variables);
        identityService.setAuthenticatedUserId("harry");
        assertEquals("Low", claim.getPriority());
        return taskService.createTaskQuery()
                .taskCandidateUser(userId)
                .list();
    }
}
