<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Claim form</title>
</head>
<body>
<h2>Please insert claim amount:</h2>
<form:form method="POST" action="/process">
    <table>
        <tr>
            <td><form:label path="amount">Set claim amount:</form:label></td>
            <td><form:input path="amount"/></td>
        </tr>
        <tr>
            <td><form:label path="group">Assign group:</form:label></td>
            <td><form:select path="group">
                <option value="denialManagement">denialManagement</option>
                <option value="personnel">personnel</option>
                <option value="administration">administration</option>
            </form:select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
