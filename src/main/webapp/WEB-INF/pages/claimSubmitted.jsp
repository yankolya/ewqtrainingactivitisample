<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Result</title>
</head>
<body>

<h2>Submitted Claim Information</h2>
<table>
    <tr>
        <td>Id</td>
        <td>${claim.id}</td>
    </tr>
    <tr>
        <td>Amount</td>
        <td>${claim.amount}</td>
    </tr>
    <tr>
        <td>Status</td>
        <td>${claim.status}</td>
    </tr>
    <tr>
        <td>Priority</td>
        <td>${claim.priority}</td>
    </tr>
</table>
<c:if test="${claim.amount>3000}">
    <form:form method="POST" action="/assignUser">
        <table>
            <tr>
                <td><form:label path="user">Assign user:</form:label></td>
                <td><form:select path="user">
                    <option value="tom">tom</option>
                    <option value="harry">harry</option>
                    <option value="michael">michael</option>
                </form:select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Submit"/>
                </td>
            </tr>
        </table>
    </form:form>
</c:if>

<a href="<c:url value="/welcome" />"> Home</a>
</body>
</html>
