<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Activiti Demo Application</title>
</head>
<body>

<c:if test="${pageContext.request.userPrincipal.name != null}">
    <h2>Welcome : ${pageContext.request.userPrincipal.name}
        | <a href="<c:url value="/j_spring_security_logout" />"> Logout</a></h2>
</c:if>
<c:forEach items="${tasks}" var="task">
    <p>${task}</p>
    <a href="<c:url value="/complete/${task.id}" />"> Complete</a>
    <br/>
</c:forEach>
<a href="<c:url value="/claim" />"> Create new process</a>
</body>
</html>