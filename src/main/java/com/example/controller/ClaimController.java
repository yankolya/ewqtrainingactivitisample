package com.example.controller;

import com.example.dto.ClaimFormDto;
import com.example.service.ClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClaimController {

    @Autowired
    public ClaimService claimService;

    @RequestMapping(value = {"/welcome", "/"}, method = RequestMethod.GET)
    public String getIndexPage(Model model) {
        model.addAttribute("page_title", "Index title");
        model.addAttribute("tasks", claimService.getTasksForCurrentUser());
        return "welcome";
    }

    @RequestMapping(value = "/claim", method = RequestMethod.GET)
    public ModelAndView getClaimPage() {
        return new ModelAndView("claim", "command", new ClaimFormDto());
    }

    @RequestMapping(value = "/process", method = RequestMethod.POST)
    public ModelAndView processClaim(@ModelAttribute ClaimFormDto processParams, Model model) {
        model.addAttribute("claim", claimService.processClaim(processParams));
        return new ModelAndView("claimSubmitted", "command", new ClaimFormDto());
    }

    @RequestMapping(value = "/assignUser", method = RequestMethod.POST)
    public String assignUser(@ModelAttribute ClaimFormDto processParams) {
        claimService.assignUser(processParams);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/complete/{taskId}", method = RequestMethod.GET)
    public String completeTask(@PathVariable String taskId) {
        claimService.completeTask(taskId);
        return "redirect:/welcome";
    }
}
