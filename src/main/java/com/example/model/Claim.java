package com.example.model;

import java.io.Serializable;
import java.util.Random;

public class Claim implements Serializable {

    private Long id;
    private String name;
    private Double amount;
    private String status;
    private String priority;

    public Claim() {
    }

    public Claim(String name, Double amount, String status) {
        this.id = Math.abs(new Random().nextLong());
        this.name = name;
        this.amount = amount;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Claim{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                '}';
    }
}
