package com.example.service;

import com.example.model.Claim;
import com.example.dto.ClaimFormDto;
import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClaimService {

    @Autowired
    public RuntimeService runtimeService;
    @Autowired
    public TaskService taskService;
    @Autowired
    public RepositoryService repositoryService;
    @Autowired
    public IdentityService identityService;
    @Autowired
    public FormService formService;

    private Map<String, Object> variables = new HashMap<String, Object>();
    private ProcessInstance processInstance;

    public Claim processClaim(ClaimFormDto processParams) {
        Claim claim = new Claim(null, processParams.getAmount(), null);
        variables.put("claim", claim);
        variables.put("group", processParams.getGroup());
        processInstance = runtimeService.startProcessInstanceByKey("claimStandardWorkflow", variables);
        return claim;
    }

    public void assignUser(ClaimFormDto processParams){
        TaskQuery taskQuery = taskService.createTaskQuery();
        String processId = processInstance.getId();
        List<Task> currentTasks = taskQuery.processInstanceId(processId).list();
        //TODO Remove hardcode
        String previousTaskId = currentTasks.get(0).getId();
        runtimeService.setVariable(processInstance.getId(), "user", processParams.getUser());
        taskService.complete(previousTaskId);
    }

    public List<Task> getTasksForCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return taskService.createTaskQuery().taskCandidateUser(authentication.getName()).list();
    }

    public void completeTask(String taskId){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        taskService.setVariable(taskId ,"user", authentication.getName());
        taskService.complete(taskId);
    }
}
