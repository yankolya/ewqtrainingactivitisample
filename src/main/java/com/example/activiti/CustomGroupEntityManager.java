package com.example.activiti;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomGroupEntityManager extends GroupEntityManager {

    private final static Group DENIAL_MANAGEMENT = new GroupEntity("denialManagement");
    private final static Group PERSONNEL = new GroupEntity("personnel");
    private final static Group ADMINISTRATION = new GroupEntity("administration");

    private final static Group[] HARRIES_GROUPS = {DENIAL_MANAGEMENT, PERSONNEL, ADMINISTRATION};
    private final static Group[] MICHAELS_GROUPS = {DENIAL_MANAGEMENT};
    private final static Group[] TOMS_GROUPS = new Group[]{};

    private final static Map<String, Group[]> GROUP_MAP = new HashMap<String, Group[]>();

    public CustomGroupEntityManager() {
        initializeData();
    }

    public void initializeData() {
        GROUP_MAP.put("harry", HARRIES_GROUPS);
        GROUP_MAP.put("michael", MICHAELS_GROUPS);
        GROUP_MAP.put("tom", TOMS_GROUPS);
    }

    public Group[] getGroupForUser(String userId) {
        Group[] groups = GROUP_MAP.get(userId);
        if (groups != null) {
            return groups;
        } else {
           return  new Group[]{};
        }
    }

    @Override
    public List<Group> findGroupsByUser(String userId) {
        if (!(userId == null)) {
            return Arrays.asList(getGroupForUser(userId));
        } else {
            throw new ActivitiException("Please specify the userId");
        }
    }

    @Override
    public Group createNewGroup(String groupId) {
        throw new ActivitiException("My group manager doesn't support creating a new group");
    }

    @Override
    public void insertGroup(Group group) {
        throw new ActivitiException("My group manager doesn't support insert a new group");
    }

    @Override
    public void updateGroup(Group updatedGroup) {
        throw new ActivitiException("My group manager doesn't support update a new group");
    }

    @Override
    public void deleteGroup(String groupId) {
        throw new ActivitiException("My group manager doesn't support delete a new group");
    }

    @Override
    public GroupQuery createNewGroupQuery() {
        throw new ActivitiException("My group manager doesn't support delete a new group");
    }

    @Override
    public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNewGroup(Group group) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
        throw new UnsupportedOperationException();
    }
}
