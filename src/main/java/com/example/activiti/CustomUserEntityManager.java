package com.example.activiti;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;

import java.util.List;
import java.util.Map;

public class CustomUserEntityManager extends UserEntityManager {

    @Override
    public UserEntity findUserById(String username) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(username);
        userEntity.setFirstName(username);
        return userEntity;
    }

    @Override
    public List<Group> findGroupsByUser(String userId) {
           throw new ActivitiException("My group manager doesn't finding groups by user");
    }

    @Override
    public Boolean checkPassword(String userId, String password) {
        return false;
    }

    @Override
    public User createNewUser(String userId) {
        throw new ActivitiException("My user manager doesn't support creating a new user");
    }

    @Override
    public void insertUser(User user) {
        throw new ActivitiException("My user manager doesn't support inserting a new user");
    }

    @Override
    public void updateUser(User updatedUser) {
        throw new ActivitiException("My user manager doesn't support updating a user");
    }

    @Override
    public void deleteUser(String userId) {
        throw new ActivitiException("My user manager doesn't support deleting a user");
    }

    @Override
    public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long findUserCountByQueryCriteria(UserQueryImpl query) {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserQuery createNewUserQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> findPotentialStarterUsers(String proceDefId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNewUser(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Picture getUserPicture(String userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setUserPicture(String userId, Picture picture) {
        throw new UnsupportedOperationException();
    }
}