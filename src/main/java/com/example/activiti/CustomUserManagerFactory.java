package com.example.activiti;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.UserIdentityManager;

public class CustomUserManagerFactory implements SessionFactory {

    public Class<?> getSessionType() {
        // original UserManager
        return UserIdentityManager.class;
    }

    public Session openSession() {
        // Customized UserManger extended from org.activiti.engine.impl.persistence.entity.UserManager
        return new CustomUserEntityManager();
    }
}
