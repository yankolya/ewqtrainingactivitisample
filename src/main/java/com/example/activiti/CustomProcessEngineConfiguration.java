package com.example.activiti;

import org.activiti.engine.impl.util.ReflectUtil;
import org.activiti.spring.SpringProcessEngineConfiguration;

import java.io.InputStream;

public class CustomProcessEngineConfiguration extends SpringProcessEngineConfiguration {

    public static final String CUSTOM_MYBATIS_MAPPING_FILE = "mybatis/custommapping.xml";

    protected InputStream getMyBatisXmlConfigurationSteam() {
        return ReflectUtil.getResourceAsStream(CUSTOM_MYBATIS_MAPPING_FILE);
    }
}
