package com.example.task;

import com.example.model.Claim;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ClaimCategorizer implements JavaDelegate{
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Claim claim = (Claim) execution.getVariable("claim");
        if(claim.getAmount() <  5000) {
            claim.setPriority("Low");
        } else if (claim.getAmount() > 5000 && claim.getAmount() < 10000) {
            claim.setPriority("Medium");
        } else {
            claim.setPriority("High");
        }

    }
}
