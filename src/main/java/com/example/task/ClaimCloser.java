package com.example.task;

import com.example.model.Claim;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ClaimCloser implements JavaDelegate {
    public void execute(DelegateExecution execution) {
        Claim claim = (Claim) execution.getVariable("claim");
        claim.setStatus("Closed");
    }
}
