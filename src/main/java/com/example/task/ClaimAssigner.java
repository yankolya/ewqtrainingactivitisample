package com.example.task;

import com.example.model.Claim;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ClaimAssigner implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Claim claim = (Claim) execution.getVariable("claim");
        if (claim.getPriority().equals("Low")) {
            execution.setVariable("user", "tom");
        } else if (claim.getPriority().equals("Medium")) {
            execution.setVariable("user", "michael");
        } else {
            execution.setVariable("user", "harry");
        }
    }
}
