# Application Set Up

https://www.evernote.com/shard/s31/sh/d3495e75-470c-4936-922a-d3bf75dba363/c4408ab074ac927a

**Note:** please use database with name *mytest* instead of *activiti*


# Application Purpose#

Application created to describe how to use basics of the Activiti Framework. 
Here is implemented one sample BPMN process which describes Claims Managment by different users

# Process description #

![workflow.png](https://bitbucket.org/repo/nG7zqd/images/3526696591-workflow.png)


### Flow ##
The main flow is next: user fills claim creation form and then based on that data application using process description knows which **flow** to choose

### Security ###
Application has 3 users:

+ tom
+ michael
+ harry

Password are the same as usernames. There are two roles in the app which maps to users as follows:

 + tom -> ROLE_NURSE
 + michael -> ROLE_NURSE
 + harry -> ROLE_DENIAL_MANAGER

### Conditions ###
Also there were implemented a few **conditional expressions** to choose direction of the process based on the claim data

### Tasks ###
There are two main task categories:

+ User Tasks - which requires user action for that the process could move on (**Clarify Claim**, **Process Claim**)
+ Service Tasks - performed by the code (**Categorize**, **Assign**, **Close**)

### Events ###
There were implemented one event called **timer boundary event** which activates when there wasn't user action on task Clarify Claim more than 15sec

### Example ###

#### 1. Harry logins in the application ####
#### 2. Harry is a ROLE_DENIAL_MANAGER so that he can create new process and he hits appropriate button ####
#### 3. Harry fills creation form as follows ####

![cc_form.png](https://bitbucket.org/repo/nG7zqd/images/2320806439-cc_form.png)

After hitting submit button process is started

#### 4. After that he can assign user to the created claim  ####

![ua_form.png](https://bitbucket.org/repo/nG7zqd/images/2805073600-ua_form.png)

#### 5. Tom logins at the application ####
#### 6. Tom see new task in his worklist ####

![wl.png](https://bitbucket.org/repo/nG7zqd/images/1093646759-wl.png)

#### 7. Tom completes the task and after that process ends ####